#ifndef BASIC_SDL_H
#define BASIC_SDL_H

//basic SDL functions
#include "Functions.h"

//timer class to control update and frame rate
#include "Timer.h"


//abstract sprite class and inherited classes
#include "Sprite.h"
#include "Dot.h"
#include "Paddle.h"
#include "Button.h"

#endif