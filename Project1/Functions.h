#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <string>
#include "SDL_ttf.h"
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_mixer.h"

//Contains basic functions useful with SDL
namespace Basic_SDL
{
	/*The apply_surface, load_file (overloaded methods are based on it) and load_image methods were originally from Lazy Foo' Productions. 
	(http://lazyfoo.net/)*/

	//Blit an SDL_Surface to another one
	void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip = NULL );

	//Initialize all necessary libraries (SDL, SDL_image, SDL_Mixer, SDL_TTF)
	bool init(SDL_Surface*& screen, int width, int height, int bpp, std::string caption);

	//Load an image to an SDL_Surface
	bool load_file(SDL_Surface*& surface, std::string file);

	//Load a font
	bool load_file(TTF_Font*& font, std::string file);

	//Load a sound effect
	bool load_file(Mix_Chunk*& sound, std::string file);

	//Load a music file
	bool load_file(Mix_Music*& music, std::string file);

	//Return a string stating on which side of rect1 there was a collision (i.e. Collision on left side of rect1 returns "LEFT")
	std::string check_collision(SDL_Rect& rect1, SDL_Rect& rect2);

	//returns an optimized SDL_Surface of the image.
	//used in load_file function
	SDL_Surface* load_image(std::string filename);
}

#endif