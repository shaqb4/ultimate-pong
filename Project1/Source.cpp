
#include "Game.h"

int main( int argc, char* args[] )
{
	//Create a Game object
	Game game;

	//Run the game
	game.Run();

    return 0;
}
