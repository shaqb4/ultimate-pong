#include "Game.h"

//Initialize the paddles, dot and the menu buttons
Game::Game(): leftPaddle(15, (SCREEN_HEIGHT-100)/2, true), rightPaddle(SCREEN_WIDTH-35, (SCREEN_HEIGHT-100)/2, false), startButton(270, 150, "img/StartButton.png", this),
	onePlayer(205, 275, "img/1Player.png", this), twoPlayer(335, 275, "img/2Player.png", this), chooseLeft(205, 275, "img/LeftPaddle.png", this),
	chooseRight(270, 275, "img/RightPaddle.png", this), restartButton(270, 400, "img/RestartButton.png", this)
{

	error = false;

	//Let the ball be "aware" of the paddles (for collision)
	myDot.setPaddles(&leftPaddle, &rightPaddle);

	//set the click response for the start button with a member function pointer
	startButton.setResponse(&Game::StartButton);
	restartButton.setResponse(&Game::Restart);
	onePlayer.setResponse(&Game::OnePlayer);
	twoPlayer.setResponse(&Game::TwoPlayer);
	chooseLeft.setResponse(&Game::ChooseLeft);
	chooseRight.setResponse(&Game::ChooseRight);
	
	//Turn these two buttons off until player decides to do 1 player (clicks OnePlayer button)
	chooseLeft.disable();
	chooseRight.disable();

	//The surfaces
	screen = NULL;
	player1Score = NULL;
	player2Score = NULL;
	title = NULL;
	winner = NULL;

	//Music during menu state
	backgroundMusic = NULL;

	//score font
	font = NULL;

	//text color black
	textColor.b = 0;
	textColor.g = 0;
	textColor.r = 0;

	quit = false; //User hasn't quit
	paused = true; //Start with the MenuState
	gameOver = false; //No one has won

	//Both players' score equals 0
	score1 = 0;
	score2 = 0;
	
	//Initialize SDL
	if (Basic_SDL::init(screen, SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, "Pong") == false)
	{
		error = true;
	}
	
	//Load the files for the dot
	if((myDot.setSurface("img/ball.png") == false) || (myDot.setCollisionSound("audio/Bounce.wav")) == false)
    {
        error = true;
    }

	//Load the music file
	if (Basic_SDL::load_file(backgroundMusic, "audio/PongSound.wav") == false)
	{
		error = true;
	}

	//load the font
	if ((Basic_SDL::load_file(font, "Fonts/AGENCYR.ttf") == false))
	{
		error = true;
	}

	//Set the images for the buttons
	if ((startButton.setSurface() == false) || (onePlayer.setSurface() == false) || (twoPlayer.setSurface() == false) || (chooseLeft.setSurface() == false) ||
		(chooseRight.setSurface() == false) || (restartButton.setSurface() == false))
	{
		error = true;
	}
}

Game::~Game()
{
		//Free the surfaces
		SDL_FreeSurface(player1Score);
		SDL_FreeSurface(player2Score);
		if (title != NULL)
		{
			SDL_FreeSurface(title);
		}
		if (winner != NULL)
		{
			SDL_FreeSurface(winner);
		}

		//Free the music
		Mix_FreeMusic(backgroundMusic);

		//Free the font
		TTF_CloseFont(font);

		//Quit TTF
		TTF_Quit();

		//Quit SDL_mixer
		Mix_CloseAudio();

		//Quit SDL
		SDL_Quit();
}

//convert number to string
std::string Game::IntToString(int number)
{
	std::stringstream ss;
	ss << number;
	return ss.str();
}

//game loop
int Game::GameState(int& keeper)
{	
	//If there's a winner, tell game to run WinState
	if (score1 == 10 || score2 == 10)
	{
		gameOver = true;
		paused = true;
	}
			
	//While there's events to handle
	while( SDL_PollEvent( &event ))
	{
		//pause game if 'p' is pressed
		if (event.type == SDL_KEYDOWN)
		{
			if (event.key.keysym.sym == SDLK_p)
			{	
				//Reset 1 player buttons
				chooseRight.disable();
				chooseLeft.disable();
				onePlayer.enable();
				paused = true;
			}
		}
				
			//Handle events for the dot and paddles
			myDot.handle_input(event);
			leftPaddle.handle_input(event);
			rightPaddle.handle_input(event);
				
				
				

		//If the user has Xed out the window
		if( event.type == SDL_QUIT )
		{
			//Quit the program
			quit = true;
		}
	}

			
	//check if a player scored and add it to the correct player
	keeper = myDot.check_score(SCREEN_WIDTH, SCREEN_HEIGHT);
	if (keeper == 1)
	{
		score1++;
	}
	else if (keeper == 2)
	{
		score2++;
	}

	//Move dot and paddles
	myDot.move(SCREEN_WIDTH, SCREEN_HEIGHT);
	leftPaddle.move(0, SCREEN_HEIGHT);
	rightPaddle.move(0, SCREEN_HEIGHT);

	//Fill the screen white
	SDL_FillRect( screen, &screen->clip_rect, SDL_MapRGB( screen->format, 0xFF, 0xFF, 0xFF ) );

	//set 
	player1Score = TTF_RenderText_Solid(font, IntToString(score1).c_str(), textColor); 
	player2Score = TTF_RenderText_Solid(font, IntToString(score2).c_str(), textColor); 

	//render scores to screen if surfaces aren't null
	if (player1Score != NULL)
	{
		Basic_SDL::apply_surface(125, 50, player1Score, screen);
	}
	if (player2Score != NULL)
	{
		Basic_SDL::apply_surface(SCREEN_WIDTH-125, 50, player2Score, screen);
	}

	//Show sot and paddles on screen
	myDot.show(screen);
	leftPaddle.show(screen);
	rightPaddle.show(screen);

	//Update the screen
	if( SDL_Flip( screen ) == -1 )
	{
		return 1;
	}
}

int Game::MenuState()
{
		//while there's events to handle
		while (SDL_PollEvent(&event))
		{

			//if 'p' is pressed, unpause game
			//Same as clicking the Start button
			if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_p)
				{
					paused = false;
				}
			}

			//Handle key presses for paddles while paused
			//Mostly so paddles know when a key is released
			leftPaddle.handle_input(event);
			rightPaddle.handle_input(event);

			//handle button clicks
			startButton.handle_input(event);
			restartButton.handle_input(event);
			onePlayer.handle_input(event);
			twoPlayer.handle_input(event);
			chooseLeft.handle_input(event);
			chooseRight.handle_input(event);

			//If the user has Xed out the window
			if( event.type == SDL_QUIT )
			{
				//Quit the program
				quit = true;
			}
		}
		//Fill screen grey
		SDL_FillRect(screen, &screen->clip_rect, SDL_MapRGB(screen->format, 169, 169, 169));

		//Render the game title
		if (title == NULL) //title is always the same, so only set it once
		{
			title = TTF_RenderText_Solid(font, "Shaq's Ultimate Pong", textColor);
		}
		if (title != NULL) //draw title only if there wasn't an error setting the surface
		{
			Basic_SDL::apply_surface((SCREEN_WIDTH/2)- (title->w/2), 50, title, screen);
		}

		//draw each button to the menu
		startButton.show(screen);
		restartButton.show(screen);
		onePlayer.show(screen);
		twoPlayer.show(screen);
		chooseLeft.show(screen);
		chooseRight.show(screen);

		//Update the screen
		if( SDL_Flip( screen ) == -1 )
		{
			return 1;
		}
	
		return 0;
}

int Game::WinState(Basic_SDL::Timer& time)
{
	//Set up win state if it hasn't been already
	if (!time.is_started())
	{
		//Display correct message depending on who won
		if (score1 == 10)
		{

			winner = TTF_RenderText_Solid(font, "Player 1 Wins!!", textColor);
		}
		else if (score2 == 10)
		{
			winner = TTF_RenderText_Solid(font, "Player 2 Wins!!", textColor);
		}

		//reset 1 player buttons
		chooseRight.disable();
		chooseLeft.disable();
		onePlayer.enable();

		//set player scores to 0
		score1 = 0;
		score2 = 0;

		//start the win state timer
		time.start();
	}

	//while there's events to handle
	while (SDL_PollEvent(&event))
	{
		//Stop WinState if left mouse button is clicked
		if (event.type == SDL_MOUSEBUTTONDOWN)
		{
			if (event.button.button == SDL_BUTTON_LEFT)
			{
				time.stop();
			}
		}

		//Handle key presses for paddles while in win state
		//Mostly so paddles know when key is released
		leftPaddle.handle_input(event);
		rightPaddle.handle_input(event);

		//If the user has Xed out the window
		if( event.type == SDL_QUIT )
		{
			//Quit the program
			quit = true;
		}
	}
	//Fill screen white
	SDL_FillRect(screen, &screen->clip_rect, SDL_MapRGB(screen->format, 0xFF, 0xFF, 0xFF));

	//render win message
	if (winner != NULL)
	{
		Basic_SDL::apply_surface((SCREEN_WIDTH/2)-(winner->w/2), (SCREEN_HEIGHT/2)-(winner->h/2), winner, screen);
	}
	
	//if 5 seconds have passed, stop WinState
	if (time.is_started() && time.get_ticks() >= 5000)
	{
		time.stop();
	}

	//if WinState is stopped, tell game to stop running WinState
	if(!time.is_started())
	{
		SDL_FreeSurface(winner); //free surface
		winner = NULL;
		gameOver = false; //Set game over to false, goes to menu state instead
	}

	//Update screen
	if (SDL_Flip(screen) == -1)
	{
		return 1;
	}

	return 0;
}


int Game::Run()
{
	//if there was an error during initialization in constructor, return 1
	if (error)
	{
		return 1;
	}

    //The frame rate regulator
    Basic_SDL::Timer fps;

	//Times how long the win state stays up, passed to WinState
	Basic_SDL::Timer winTimer;

	//Which player scored (1-left or 2-right), passed to GameState
	int scoreKeeper = 0;

	//set right paddle to use ai as default
	rightPaddle.setAI(true);
	
	//While the user hasn't quit
	while (quit == false)
	{
		//Start the timer
		fps.start();

		//if the game isn't paused, stop any playing music and run the gamestate
		if (!paused)
		{
			if (Mix_PlayingMusic() != 0)
			{
				Mix_HaltMusic();
			}
			GameState(scoreKeeper);
		}
		else //if the game is paused
		{
			//if the game is over (a player won), stop any playing music and 
			//run the win state
			if (gameOver)
			{
				if (Mix_PlayingMusic() != 0)
				{
					Mix_HaltMusic();
				}
				WinState(winTimer);
			}
			else
			{
				//if the game is paused but not over, play music and run the menu state
				if (Mix_PlayingMusic() == 0)
				{
					if (Mix_PlayMusic(backgroundMusic, -1) == -1)
					{
						return 1;
					}
				}
				MenuState();
			}
		}
		//regulate the framerate
		if( fps.get_ticks() < 1000 / FRAMES_PER_SECOND )
		{
			SDL_Delay( ( 1000 / FRAMES_PER_SECOND ) - fps.get_ticks() );
		}
	}
}

/**
*	resume the game
**/
void Game::StartButton(SDL_Event& event)
{
	if (event.type == SDL_MOUSEBUTTONDOWN)
	{
		if (event.button.button == SDL_BUTTON_LEFT)
		{
			paused = false;
		}
	}
}

/**
*	Disable the OnePlayer button and enable the buttons alowing the 
*	player to choose which paddle
**/
void Game::OnePlayer(SDL_Event& event)
{
	if (event.type == SDL_MOUSEBUTTONDOWN)
	{
		if (event.button.button == SDL_BUTTON_LEFT)
		{
			onePlayer.disable();
			chooseLeft.enable();
			chooseRight.enable();
		}
	}
}

/**
*	Set the right paddle AI to true and left paddle AI to false
*	Then restart the game
**/
void Game::ChooseLeft(SDL_Event& event)
{
	if (event.type == SDL_MOUSEBUTTONDOWN)
	{
		if (event.button.button == SDL_BUTTON_LEFT)
		{
				leftPaddle.setAI(false);
				rightPaddle.setAI(true);

				Restart(event);
		}
	}
}

/**
*	Set the left paddle AI to true and right paddle AI to false
*	Then restart the game
**/
void Game::ChooseRight(SDL_Event& event)
{
	if (event.type == SDL_MOUSEBUTTONDOWN)
	{
		if (event.button.button == SDL_BUTTON_LEFT)
		{
			leftPaddle.setAI(true);
			rightPaddle.setAI(false);

			Restart(event);
		}
	}
}

/**
*	Set the AI for both paddles to false
	Then restart the game
**/
void Game::TwoPlayer(SDL_Event& event)
{
	if (event.type == SDL_MOUSEBUTTONDOWN)
	{
		if (event.button.button == SDL_BUTTON_LEFT)
		{
			leftPaddle.setAI(false);
			rightPaddle.setAI(false);

			Restart(event);
		}
	}
}

/**
*	Put dot and paddles in start locations and reset the scores
**/
void Game::Restart(SDL_Event& event)
{
	if (event.type == SDL_MOUSEBUTTONDOWN)
	{
		if (event.button.button == SDL_BUTTON_LEFT)
		{
			myDot.setXVel(0);
			myDot.setYVel(0);
			myDot.setX(310);
			myDot.setY(265);

			rightPaddle.setY(SCREEN_HEIGHT/2-(rightPaddle.getHeight()/2));
			rightPaddle.setYVel(0);
			leftPaddle.setY(SCREEN_HEIGHT/2-(leftPaddle.getHeight()/2));
			leftPaddle.setYVel(0);

			score1 = 0;
			score2 = 0;

			paused = false;
		}
	}
}

