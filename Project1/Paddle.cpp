#include "Paddle.h"

namespace Basic_SDL
{

	//Maximum speed of the paddle: 5
	const int Paddle::SPEED = 5;

	Paddle::Paddle(int X, int Y, bool side)
	{
		//AI is false by default
		aI = false;

		//where to go if AI
		yDest = 0;

		//Set location
		x = X;
		y = Y;

		//which side this paddle is on
		left = side;

		//counts how many input keys are down
		key_down = 0;

		height = 100;
		width = 20;

		yVel = 0;
		xVel = 0;
	}

	void Paddle::handle_input(SDL_Event& event)
	{
		if (!aI) //only if the paddle isn't AI
		{
			if (!left) //if right paddle, get correct input
			{
				if (event.type == SDL_KEYDOWN) //if a key is pressed
				{
					switch (event.key.keysym.sym)
					{
					case SDLK_UP: //if up arrow key is pressed
						//increment number of keys down and change speed accordingly
						key_down++;
						yVel -= SPEED*key_down;
						break;
					case SDLK_DOWN: //if down arrow key is pressed
						//increment number of keys down and change speed accordingly
						key_down++;
						yVel += SPEED*key_down;
						break;
					}
				}
				else
				{
					if (event.type == SDL_KEYUP) //if key is released
					{
						switch (event.key.keysym.sym)
						{
						case SDLK_UP: //if up arrow key is released
							//if up and down are pressed and the paddle is going up, make it go down if up key is released
							if ((key_down == 2) && (yVel < 0))
							{
								yVel += SPEED*key_down;
							}
							key_down--; //decrement number of keys pressed		
							break;
						case SDLK_DOWN: //if down arrow key is released
							//if up and down are pressed and the paddle is going down, make it go up if down key is released
							if ((key_down == 2) && (yVel > 0))
							{
								yVel -= SPEED*key_down;
							}
							key_down--;	//decrement number of keys down
							break;
						}
						//if no keys are down, set velocity to 0
						if (!key_down)
						{
							yVel = 0;
						}
					}
				}
			}	
			else //if the paddle is on the left, handle the input the same way except with W/S
			{
				if (event.type == SDL_KEYDOWN)
				{
					switch (event.key.keysym.sym)
					{
					case SDLK_w:
							key_down++;
							yVel -= SPEED*key_down;
						break;
					case SDLK_s:
							key_down++;
							yVel += SPEED*key_down;
						break;
					}
				}
				else
				{
					if (event.type == SDL_KEYUP)
					{
						switch (event.key.keysym.sym)
						{
						case SDLK_w:
							if ((key_down == 2) && (yVel < 0))
							{
								yVel += SPEED*key_down;
							}
							key_down--;		
							break;
						case SDLK_s:
							if ((key_down == 2) && (yVel > 0))
							{
								yVel -= SPEED*key_down;
							}
							key_down--;
							break;
						}
						if (!key_down)
						{
							yVel = 0;
						}
					}
				}
			}
		}
	}

	void Paddle::handle_AI(int dotX, int dotY, int dotXVel, int dotYVel)
	{
		double updates = (abs(dotX-x)-20)/abs(dotXVel);//# frames updated while dot travels between paddles
		double yDistance = updates*dotYVel;//vertical distance traveled by dot
		double yPoint = dotY + yDistance + 10;//final y coordinate where dot will pass/hit paddle
		yDest = yPoint; //set y-destination of paddle

		//if destination is below the paddle, make the paddle go down
		if (yDest > y+height)
		{
			yVel = SPEED;
		}
		else if (yDest < y)  //if the destination is above the paddle, make the paddle go up 
		{
			yVel = -SPEED;
		}
		else //otherwise don't move the paddle
		{
			yVel = 0;
		}
	}

	void Paddle::move(int x_max, int y_max)
	{
			srand(SDL_GetTicks()); //seed random num generator

			//if still in the screen, move the paddle
			if ((y > 0) && (y + height < y_max))
			{
				y += yVel;
			}
			else if (y <= 0) //if past top of screen, get it out
			{
				y++;
			
			}
			else //if past bottim of screen, get it out
			{
				y--;
			
			}

			//if the paddle is AI, stop it when it crosses the destination
			if (aI)
			{
				//stop the paddle almost immedietely after crossing the destination
				//increase the mod value to make the game less difficult i.e. change 5 to 20
				if ((yDest > y+rand()%5) && (yDest < y+height-rand()%5))
				{
					yVel = 0;
				}
			}
	
	}

	void Paddle::show(SDL_Surface*& screen)
	{
		//make a rectangle to fill
		SDL_Rect rect; 
		rect.x = this->x;
		rect.y = this->y;
		rect.w = this->width;
		rect.h = this->height;

		//fill the rectangle black
		SDL_FillRect(screen, &rect, SDL_MapRGB(screen->format, 0, 0, 0));
	}

	//return the speed of paddles
	const int Paddle::getMaxSpeed()
	{
		return SPEED;
	}

	//whether the paddle is AI
	bool Paddle::isAI() const
	{
		return aI;
	}

	//Set the AI to true or false
	void Paddle::setAI(bool ai)
	{
		aI = ai;
	}
}