#ifndef BUTTON_H
#define BUTTON_H

#include "SDL.h"
#include "Sprite.h"
#include <string>

class Game;
namespace Basic_SDL
{	
	class Button: public Sprite
	{
	private:
		//Game this button is a part of
		Game& game;
	
		//if mouse is over button
		bool mouseOver;

		//turn button on/off
		bool isEnabled;

		SDL_Rect clip;

		//Image for button, should be tile sheet of default image
		//and then hover image right next to each other
		SDL_Surface* button;

		std::string file; //name of image file

		//Callback function
		void (Game::*onClick) (SDL_Event&);

	public:
		//Initialize button location, image file, and Game it belongs to
		Button(int X, int Y, std::string File, Game* game);

		//Free the button image surface
		~Button();

		//Set image of button
		bool setSurface();

		//set the event handler
		void setResponse(void (Game::*onClick) (SDL_Event&));

		//respond to mouse clicks
		void handle_input(SDL_Event& event);

		//Moves the dot
		void move(int x_max, int y_max) {}

		//Shows the dot on the screen
		void show(SDL_Surface*& screen);

		//Turn button on
		void enable() { isEnabled = true; mouseOver = false; }

		//Turn button off
		void disable() { isEnabled = false; mouseOver = false; }

		//Whether button is on or off
		bool isOn() { return isEnabled; }

	};
}

#endif