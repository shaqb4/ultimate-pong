#ifndef DOT_H
#define DOT_H

#include "Functions.h"
#include "Sprite.h"
#include "Paddle.h"

//The dot that will move around on the screen
namespace Basic_SDL
{
	class Dot: public Sprite
	{
	private:
		//left and right paddle to check collision with
		Paddle* leftPaddle;
		Paddle* rightPaddle;

		//collision sound
		Mix_Chunk* bounce;

		//image of dot
		SDL_Surface* dot;
	public:
		//Initializes the variables
		Dot();

		//Free the collision sound and dot image.
		//Set paddle pointers to NULL
		~Dot();

		//returns SDL_Surface of dot
		SDL_Surface*& getSurface(); 
	
		//set the image of the surface
		bool setSurface(std::string file);

		//set the sound when the dot hits a paddle
		bool setCollisionSound(std::string file);

		//Takes key presses and adjusts the dot's velocity
		void handle_input(SDL_Event& event);

		//Moves the dot
		void move(int x_max, int y_max);

		//Check if the dot has passed a paddle, and returns which one
		int check_score(int x_max, int y_max);

		//Shows the dot on the screen
		void show(SDL_Surface*& screen);

		//Initialize pointers to left and right paddles
		void setPaddles(Paddle* paddle1, Paddle* paddle2);

	};
}
#endif