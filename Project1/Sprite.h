#ifndef SPRITE_H
#define SPRITE_H

#include <string>
#include "SDL.h"

	/*The 3 virtual void functions in this class, while not implemented the same way, were originally from Lazy Foo' Productions. 
	(http://lazyfoo.net/)*/

namespace Basic_SDL
{
	class Sprite
	{
		protected:
		//The X and Y offsets of the dot
		double x, y;

		//The velocity of the dot
		int yVel, xVel;

		//dimensions of bounding box
		int width, height;

		public:

		//Takes key presses and adjusts the dot's velocity
		virtual void handle_input(SDL_Event& event) = 0;

		//Moves the dot
		virtual void move(int x_max, int y_max) = 0;

		//Shows the dot on the screen
		virtual void show(SDL_Surface*& screen) = 0;

		//returns the bounding box of this sprite as a SDL_Rect
		SDL_Rect getBox()
		{
			SDL_Rect rect; 
			rect.x = this->x;
			rect.y = this->y;
			rect.w = this->width;
			rect.h = this->height;

			return rect;
		}

		//Set the Y-velocity
		void setYVel(int vel) { yVel = vel; }

		//Get the Y-velocity
		int getYVel() const { return yVel; }

		//Set the X-velocity
		void setXVel(int vel) { xVel = vel; }

		//Get the X-velocity
		int getXVel() const { return xVel; }
	
		//get height
		int getHeight() const { return height; }

		//get width
		int getWidth() const { return width; }

		//Get x position
		int getX() const { return x; }

		//Set x position
		void setX(int pos) { x = pos; }
	
		//Get x position
		int getY() const { return y; }

		//Set y position
		void setY(int pos) { y = pos; }
	};
}
#endif