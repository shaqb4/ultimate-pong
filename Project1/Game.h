#ifndef GAME_H
#define GAME_H

	//The headers
	#include "Basic_SDL.h"
	#include <sstream>

class Game
{
private:

	/**
	*	Any general game data used by all states
	**/

		//if error
		bool error;

		//The screen attributes
		const static int SCREEN_WIDTH = 640;
		const static int SCREEN_HEIGHT = 550;
		const static int SCREEN_BPP = 32;

		//The frame rate
		const static int FRAMES_PER_SECOND = 60;
    
		//Quit flag
		bool quit;
    
		//If the game is paused
		bool paused;
	
		//if a player won
		bool gameOver;
	
		//The event structure
		SDL_Event event;

		//Screen to draw on
		SDL_Surface* screen;

		//Left and right paddles
		Basic_SDL::Paddle leftPaddle;
		Basic_SDL::Paddle rightPaddle;

		//score font
		TTF_Font* font;

		//text color
		SDL_Color textColor;
	
	/**
	*	Data used by game state
	**/
	
		//scores (used in winState as well)
		int score1, score2;
	
		//The surfaces to render the scores
		SDL_Surface* player1Score;
		SDL_Surface* player2Score;

		//Ping pong ball
		Basic_SDL::Dot myDot;

	/**
	*	Data used by menu state
	**/

		//menu state title
		SDL_Surface* title;

		//Menu music
		Mix_Music* backgroundMusic;

		//Start button
		Basic_SDL::Button startButton; //resume game
		Basic_SDL::Button restartButton; //reset the game with the current settings (i.e. 1 player, 2 player)
		Basic_SDL::Button onePlayer; //Allows you to choose which paddle to use
		Basic_SDL::Button chooseLeft; //Choose the left paddle for 1 player
		Basic_SDL::Button chooseRight; //Choose the right paddle for 1 player
		Basic_SDL::Button twoPlayer; //Make game two player by turning off paddle AIs
		
	/**
	*	Data used by win state
	**/

		//win state text
		SDL_Surface* winner;		


	//Convert an integer into a string
	std::string IntToString(int number);

public:
	//Initialize all game data needed for game, all libraries and load the necessary files
	Game();

	//Free all the SDL_Surfaces, fonts and sounds. Close libraries 
	~Game();

	//Run the correct gamestate
	int Run();

	//Handles input, logic and rendering of game state
	//keeper - keeps track of who scored (1 for left, 2 for right)
	int GameState(int& keeper);

	//Handles input, logic and rendering for menu state
	int MenuState();

	//Handles input, logic and rendering for win state
	//time - keeps track of how long the win state has been running, stops it if over 5 sec
	int WinState(Basic_SDL::Timer& time);

	//Returns whether the game is paused
	bool isPaused() const;

	//Button functions

	//Resume the current game
	void StartButton(SDL_Event& event);

	//Allows user to choose which paddle to use. Turns on AI for opposite paddle
	void OnePlayer(SDL_Event& event);

	//Play two player, turns off all AI
	void TwoPlayer(SDL_Event& event);

	//Use left paddle for 1 player, turn on AI for right paddle
	void ChooseLeft(SDL_Event& event);

	//Use right paddle for 1 playerm turn on AI for left paddle
	void ChooseRight(SDL_Event& event);

	//Reset the score and paddle/dot locations
	//keeps current settings ( i.e.  1 player, two player)
	void Restart(SDL_Event& event);
};

#endif