#include "Button.h"
#include "Functions.h"

namespace Basic_SDL
{
	Button::Button(int X, int Y, std::string File, Game* gme) : game (*gme) //Set what Game the button belongs to
	{
		button = NULL; //Set image to NULL

		//Button is turned on by default
		isEnabled = true; 

		//Mouse isn't hovering over the button
		mouseOver = false;

		//Set position
		x = X;
		y = Y;

		//Set name of image file
		file = File;

		//Default image clip size (for (non)hover state)
		height = 100;
		width = 100;
		

		//Button doesn't move
		xVel = 0;
		yVel = 0;

		//Set current button clip from image
		clip.x = 0;
		clip.y = 0;
		clip.w = width;
		clip.h = height;
	}

	Button::~Button()
	{
		//Free button image
		SDL_FreeSurface(button);

		button = NULL;
	}

	bool Button::setSurface()
	{
		//false if error occurs
		bool worked;

		//If image already set, free it
		if (button != NULL)
		{
			SDL_FreeSurface(button);
			button = NULL;
		}

		//check if error loading file
		worked = Basic_SDL::load_file(button, file.c_str());

		//change clip size of button by using the loaded image
		if (worked)
		{
			width = button->w/2;
			height = button->h;
			clip.w = button->w/2;
			clip.h = button->h;
		}

		return worked;
	}

	void Button::setResponse(void (Game::*handler) (SDL_Event&))
	{
		//Set the event handler for this button
		onClick = handler;
	}

	void Button::handle_input(SDL_Event& event)
	{
		if (isEnabled) //If the button is turned on
		{
			if (event.type == SDL_MOUSEMOTION)
			{
				//Get the mouse offsets
				int mouseX = event.motion.x;
				int mouseY = event.motion.y;
        
				//If the mouse is over the button
				if( ( mouseX > x ) && ( mouseX < x + width ) && ( mouseY > y ) && ( mouseY < y + height ) )
				{
					//Set the button sprite
					clip.x = width;
					mouseOver = true;
				}
				else
				{
					//Set the button sprite
					clip.x = 0;
					mouseOver = false;
				} 
			}

			if (mouseOver) //if mouse is hovering over the button
			{
				//handle mouse events
				(game.*onClick)(event);
			}
		}
	}


	void Button::show(SDL_Surface*& screen)
	{
		if (isEnabled) //If button is turned on
		{
			//Show the button
			Basic_SDL::apply_surface( x, y, button, screen, &clip);
		}
	}
}