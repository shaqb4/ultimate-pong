#include "Functions.h"

	/*The apply_surface, load_file (overloaded methods are based on it) and load_image methods were originally from Lazy Foo' Productions. 
	(http://lazyfoo.net/)*/

namespace Basic_SDL
{
	SDL_Surface* Basic_SDL::load_image(std::string filename)
	{
		//The image that's loaded
		SDL_Surface* loadedImage = NULL;

		//The optimized surface that will be used
		SDL_Surface* optimizedImage = NULL;

		//Load the image
		loadedImage = IMG_Load( filename.c_str() );

		//If the image loaded
		if( loadedImage != NULL )
		{
			//Create an optimized surface
			optimizedImage = SDL_DisplayFormat( loadedImage );

			//Free the old surface
			SDL_FreeSurface( loadedImage );
		}

		//Return the optimized surface
		return optimizedImage;
	}
	

	void Basic_SDL::apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip)
	{
		//Holds offsets
		SDL_Rect offset;

		//Get offsets
		offset.x = x;
		offset.y = y;

		//Blit
		SDL_BlitSurface( source, clip, destination, &offset );
	}

	bool Basic_SDL::init(SDL_Surface*& screen, int width, int height, int bpp, std::string caption)
	{
		//Initialize all SDL subsystems
		if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
		{
			return false;
		}

		//Set up the screen
		screen = SDL_SetVideoMode( width, height, bpp, SDL_SWSURFACE );

		//If there was an error in setting up the screen
		if( screen == NULL )
		{
			return false;
		}

		//if there was an error initializing TTF
		if( TTF_Init() == -1 )
		{
			return false;    
	    }

		//if there was an error initializing SDL_Mixer
		if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 1024 ) == -1 )
		{
			return false;    
		}

		//Set the window caption
		SDL_WM_SetCaption( caption.c_str(), NULL );

		//If everything initialized fine
		return true;
	}

	bool Basic_SDL::load_file(SDL_Surface*& surface, std::string file)
	{
		//Load the dot image
		surface = load_image(file);

		//If there was a problem in loading the dot
		if( surface == NULL )
		{
			return false;
		}

		//If everything loaded fine
		return true;
	}

	bool Basic_SDL::load_file(TTF_Font*& font, std::string file)
	{
		//Load the dot image
		font = TTF_OpenFont(file.c_str(), 45);

		//If there was a problem in loading the dot
		if(font == NULL )
		{
			return false;
		}

		//If everything loaded fine
		return true;
	}

	bool Basic_SDL::load_file(Mix_Chunk*& chunk, std::string file)
	{
		//load sound effect
		chunk = Mix_LoadWAV(file.c_str());

		//return false if load failed
		if (chunk == NULL)
		{
			return false;
		}

		return true;
	}

	bool Basic_SDL::load_file(Mix_Music*& music, std::string file)
	{
		//load music file
		music = Mix_LoadMUS(file.c_str());

		//return false if load failed
		if (music == NULL)
		{
			return false;
		}

		return true;
	}



	std::string Basic_SDL::check_collision(SDL_Rect& rect1, SDL_Rect& rect2)
	{
		//Return "LEFT" if left side of rect1 hit right side of rect2
		if ((rect1.x <= rect2.x+rect2.w) && (rect1.x >= rect2.x+(rect2.w/2)) && (rect1.y+rect1.h >= rect2.y) && (rect1.y <= rect2.y+rect2.h))
		{
			return "LEFT";
		}

		//return "RIGHT" if right side of rect1 hit left side of rect2
		if ((rect1.x+rect1.w >= rect2.x) && (rect1.x+rect1.w <= rect2.x+(rect2.w/2)) && (rect1.y+rect1.h >= rect2.y) && (rect1.y <= rect2.y+rect2.h))
		{
			return "RIGHT";
		}
		
		//no collision
		return "NONE";
	}
}