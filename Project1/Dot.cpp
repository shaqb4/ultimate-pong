#include "Dot.h"

namespace Basic_SDL
{
	Dot::Dot()
	{
		//Initialize the offsets
		x = 310;
		y = 265;

		//set width and height
		width = 0;
		height = 0;

		//Initialize the velocity
		xVel = 0;
		yVel = 0;

		//initialize pointers
		leftPaddle = NULL;
		rightPaddle = NULL;
		dot = NULL;

		bounce = NULL;
	}

	Dot::~Dot()
	{
		//free dot image
		SDL_FreeSurface(this->dot);
		this->dot = NULL;

		//set pointers to NULL
		this->leftPaddle = NULL;
		this->rightPaddle = NULL;

		//free collision sound
		Mix_FreeChunk(bounce);
		this->bounce = NULL;
	}

	//return dot surface
	SDL_Surface*& Dot::getSurface()
	{
		return dot;
	}

	//set the image for the dot surface
	bool Dot::setSurface(std::string file)
	{
		//set to false if error occurs
		bool worked;

		//if image already set, free it
		if (dot != NULL) 
		{
			SDL_FreeSurface(dot);
			dot = NULL;
		}

		//check if error loading file
		worked = Basic_SDL::load_file(dot, file.c_str());

		if (worked)
		{
			//set dot size to image size
			width = dot->w;
			height = dot->h;
		}

		return worked;
	}

	bool Dot::setCollisionSound(std::string file)
	{
		//set to false if error occurs
		bool worked;

		//if collision sound already set, free it
		if (bounce != NULL)
		{
			Mix_FreeChunk(bounce);
			bounce = NULL;
		}

		//check if error loading file
		worked = Basic_SDL::load_file(bounce, file.c_str());

		return worked;
	}


	void Dot::handle_input(SDL_Event& event)
	{
		//start when space is pressed
		if (event.type == SDL_KEYDOWN)
		{
			if (xVel == 0) //if the dot isn't moving
			{
				if (event.key.keysym.sym == SDLK_SPACE) //if space pressed
				{
					yVel = rand()%7; //make y-velocity of dot a random number between 0 and 6
					for (int i = 0; i < abs(yVel); i++) //make y-velocity negative or positive
					{
						yVel *= -1;
					}

					if (rightPaddle->isAI()) 
					{
						xVel = 7; //go towards right paddle
						rightPaddle->handle_AI(x, y, xVel, yVel); //handle right paddle AI
					}
					else if (leftPaddle->isAI())
					{
						xVel = -7; //go towards left paddle
						leftPaddle->handle_AI(x, y, xVel, yVel); //handle left paddle AI
					}
					else //if 2 player
					{
						//go towards right paddle with 0 y-velocity
						yVel = 0;
						xVel = 7;
					}
				}
			}
		}
	}

	void Dot::move(int x_max, int y_max)
	{

		//Move the dot left or right
		x += xVel;

		//check for collisions with either paddle
		std::string collision = Basic_SDL::check_collision(this->getBox(), leftPaddle->getBox());
		std::string collision2 = Basic_SDL::check_collision(this->getBox(), rightPaddle->getBox());
	
  
		//Move the dot up or down
		y += yVel;
	

		//If the dot went too far up or down
		if( ( y < 0 ) || ( y + height > y_max ))
		{
			//move back
			yVel = -yVel;
			if (rightPaddle->isAI() && (xVel > 0)) //if right paddle is AI and the dot is moving towards it
			{
				rightPaddle->handle_AI(x, y, xVel, yVel); //Handle right paddle AI
			}
			if (leftPaddle->isAI() && (xVel < 0)) //if left paddle is AI and the dot is moving towards it
			{
				leftPaddle->handle_AI(x, y, xVel, yVel); //Handle left paddle AI
			}
		}

		//If collsion occurred
		if((collision == "LEFT") || (collision2 == "RIGHT"))
		{
			//play collision sound
			Mix_PlayChannel(-1, bounce, 0);
		
			//bounce back
			if (collision == "LEFT")
			{
				x = 35; //move dot out of paddle

				//make dot y-velocity change depending on where it hits the paddle (max of 7)
				//i.e. hits paddle 14 pixels above center of paddle, ball goes upward with speed of 2
				yVel = (((this->getBox().y+height/2) - (leftPaddle->getBox().y + leftPaddle->getBox().h/2)) / 7);
			
				if (rightPaddle->isAI())
				{
					rightPaddle->handle_AI(x, y, xVel, yVel); //handle right paddle AI
				}
			
			}
			if (collision2 == "RIGHT")
			{
				x = x_max-width-35; //move dot out of paddle

				//make dot y-velocity change depending on where it hits the paddle (max of 7)
				//i.e. hits paddle 14 pixels above center of paddle, ball goes upward with speed of 2
				yVel = (((this->getBox().y+height/2) - (rightPaddle->getBox().y + rightPaddle->getBox().h/2)) / 7);
			
				if (leftPaddle->isAI())
				{
					leftPaddle->handle_AI(x, y, xVel, yVel); //handle left paddle AI
				}
			}

			//change dot direction
			xVel = -xVel;
		}
	}

	int Dot::check_score(int x_max, int y_max)
	{
	
	
	
		//return 2 if player 2 scores
		if (( x < 0 ))
		{
			//set dot back to center of screen
			xVel = 0;
			yVel = 0;
			x = 310;
			y = 265;

			//reset paddle positions
			rightPaddle->setY(y_max/2-(rightPaddle->getHeight()/2));
			rightPaddle->setYVel(0);
			leftPaddle->setY(y_max/2-(leftPaddle->getHeight()/2));
			leftPaddle->setYVel(0);

			return 2;
		}
		//return 1 if player 1 scores
		else if (( x + width > x_max ))
		{
			//set dot back to center of screen
			xVel = 0;
			yVel = 0;
			x = 310;
			y = 265;

			//reset paddle positions
			rightPaddle->setY(y_max/2-(rightPaddle->getHeight()/2));
			rightPaddle->setYVel(0);
			leftPaddle->setY(y_max/2-(leftPaddle->getHeight()/2));
			leftPaddle->setYVel(0);

			return 1;
		}

		//no one scored
		return 0;
	}

	void Dot::show(SDL_Surface*& screen)
	{
		//Show the dot
		Basic_SDL::apply_surface( x, y, dot, screen );
	}

	void Dot::setPaddles(Paddle* paddle1, Paddle* paddle2)
	{
		//set pointers to the left and right paddles
		leftPaddle = paddle1;
		rightPaddle = paddle2;
	}
}