#ifndef PADDLE_H
#define PADDLE_H

#include "Sprite.h"

namespace Basic_SDL
{
	class Paddle: public Sprite
	{
	private:
		//If AI or not
		bool aI;

		//Point for paddle to go if ai
		int yDest;

		//true if left paddle
		bool left;

		//how many keys are down
		int key_down;

		//speed
		static const int SPEED;

	public:
		//Initializes the variables
		Paddle(int X, int Y, bool side);

		//Takes key presses and adjusts the dot's velocity
		void handle_input(SDL_Event& event);

		//Handles paddle ai
		void handle_AI(int dotX, int dotY, int dotXVel, int dotYVel);

		//Moves the dot
		void move(int x_max, int y_max);

		//Shows the dot on the screen
		void show(SDL_Surface*& screen);

		//get paddles max speed
		const int getMaxSpeed();

		//Whether paddle is ai or not
		bool isAI() const;

		//set AI flag
		void setAI(bool ai);
	};
}
#endif